#!/bin/bash
DIR=$(dirname "$(readlink -f "$0")")
docker run --rm -it -p 9990:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin\
	-v $DIR/iqiper:/opt/jboss/keycloak/themes/iqiper \
	-v $DIR/standalone.xml:/opt/jboss/keycloak/standalone/configuration/standalone.xml\
	-v $DIR/standalone-ha.xml:/opt/jboss/keycloak/standalone/configuration/standalone-ha.xml\
	quay.io/keycloak/keycloak
